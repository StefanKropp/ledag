/*!
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @file main.cpp
 *
 * @copyright
 * Copyright (C) 2020 - 2021 Stefan Kropp <stefan.kropp@posteo.de>
 * This file is part of ledag.
 *
 * ledag is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ledag is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ledag. If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include <iostream>

#include "config.hpp"
#include "gtkgemini.hpp"
#include <gtkmm.h>

/* Glade UI file */

#define GLADE_FILE "ui.glade"
#define GLADE_ID_APPWIN "ApplicationWindow"

#ifdef PACKAGE_STATUS_DEVELOPMENT
#define GLADE_FILE_PATH "ui/" GLADE_FILE
#else
#define GLADE_FILE_PATH UI_PATH "/" GLADE_FILE
#endif

int main(int argc, char *argv[]) {

  Glib::RefPtr<Gtk::Application> app =
      Gtk::Application::create("org.codeberg.Leda.ledag");
  auto refBuilder = Gtk::Builder::create();
  try {
    refBuilder->add_from_file(GLADE_FILE_PATH);
  } catch (const Glib::FileError &ex) {
    std::cerr << "FileError: " << ex.what() << std::endl;
    return EXIT_FAILURE;
  } catch (const Glib::MarkupError &ex) {
    std::cerr << "MarkupError: " << ex.what() << std::endl;
    return EXIT_FAILURE;
  } catch (const Gtk::BuilderError &ex) {
    std::cerr << "BuilderError: " << ex.what() << std::endl;
    return EXIT_FAILURE;
  }

  Gtk::ApplicationWindow *pApplicationWindow = nullptr;
  refBuilder->get_widget<Gtk::ApplicationWindow>(GLADE_ID_APPWIN,
                                                 pApplicationWindow);
  if (!pApplicationWindow) {
    std::cerr << "Could not get the application window" << std::endl;
    return EXIT_FAILURE;
  }

  GtkGemini gtkgemini(app, refBuilder);
  Gtk::Button *pButton = nullptr;
  refBuilder->get_widget("loadPageButton", pButton);
  if (pButton) {
    pButton->signal_clicked().connect(
        sigc::mem_fun(&gtkgemini, &GtkGemini::on_clicked_load_page));
  }
  Gtk::ToolButton *pToolButton = nullptr;
  refBuilder->get_widget("showAboutButton", pToolButton);
  if (pToolButton) {
    pToolButton->signal_clicked().connect(
        sigc::mem_fun(&gtkgemini, &GtkGemini::on_clicked_show_about));
  }
  refBuilder->get_widget("showBookmarksButton", pToolButton);
  if (pToolButton) {
    pToolButton->signal_clicked().connect(
        sigc::mem_fun(&gtkgemini, &GtkGemini::on_clicked_show_bookmarks));
  }
  return app->run(*pApplicationWindow, argc, argv);
}
